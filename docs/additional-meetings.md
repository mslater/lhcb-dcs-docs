# Additional Meetings

As well as the LHCb Operations meetings, there are a few other meetings that it is the responsibility of the DCS to attend on behalf
of the LHCb Computing Team.

## Weekly WLCG Operations Meeting

There is a weekly WLCG Operations Meeting at 3pm (CERN time) every Monday. This is a short (~15mins) meeting that takes updates from the experiments and major sites as well as WLCG and related services. Information including the agenda and call in info can be found at the appropriate page on this TWiki entry: 

<https://twiki.cern.ch/twiki/bin/view/LCG/WLCGOperationsMeetings>.

For LHCb, the DCS must update the Twiki:

<https://twiki.cern.ch/twiki/bin/view/LHCb/ProductionOperationsWLCGdailyReports>

with any issues or points that are to be raised from the LHCb point of view by 1pm (CERN time). It will then be copied to the Twiki agenda for that day's meeting. It is a good idea to ask in that morning's Operations Meeting if there is anything specific to bring up. 

During the meeting, you will be asked to briefly go over the report that was submitted and may also be asked to comment on any specifc LHCb related questions. Other than this, you should take notes of any interventions, updates, dicussions, etc. that go on that specifically relate to LHCb. You should then report back on these at the Wednesday Operations meeting or email the `lhcb-grid` list if a more rapid answer is required.

## Monthly WLCG Coordination Meeting

On the first Thursday of every month, there is a WLCG Coordination meeting at 3pm (CERN time). A reminder with details of the Agenda and location of minutes will be sent out beforehand. This meeting is used to dicuss overall subjects of WLCG Operations and Management. It is recommended to ask in the previous LHCb Operations Meeting(s) and/or in the Mattermost channel for any input to the meeting besides a general status update for LHCb (see previous meeting minutes for examples). 

Due to the nature of the discussions in the meeting, it's quite likely there will be things to pass on to the LHCb Computing Team or get feedback about from them. Consequently, though minutes will be available after the meeting it's a good idea to make notes on the main topics to make sure you can pass on the appropriate information. After the meeting, as with the weekly WLCG meeting, pass on any appropriate information either at the Ops meeting, the Mattermost channel and/or the `lhcb-grid` list.

[Run Meeting? Others?]