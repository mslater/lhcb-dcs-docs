# LHCb Distributed Computing Shifters

Welcome to the **LHCb Distributed Computing Shifter Documentation** pages. 

Here you can find tutorials and guides written and reviewed by experts.

For comments and suggestions, please feel free to use the [issue tracker](https://gitlab.cern.ch/lhcb-dirac/lhcb-dcs-docs/-/issues).


If you want to update or modify the content yourself, please feel free to follow the [contribution guide](https://gitlab.cern.ch/lhcb-dirac/lhcb-dcs-docs/-/blob/master/CONTRIBUTING.md).

--- 

## Shift Organisation

A Distributed Computing Shifter (from now on DCS) shift lasts for a full week from Monday to Sunday. They are expected to monitor
the systems and respond to tickets, etc. at least through normal office hours (i.e. 9am-5pm) though a delay in response of 1-2 hours
is allowed due to other responsibilities. The shifter is not required to monitor and respond to things late into the evening or early morning.

## Prerequisites

As well as having a working knowledge of the DIRAC system and the LHCb Computing Model, the following are required to
properly perform the shift:

- a valid and working CERN computing account
- a valid Grid Certificate that is registered in the with the LHCb VO
- Authorisation for the `lhcb_shifter` role
- Is subscribed to the appropriate mailing lists
- Has an account registered with the [ELog](https://lblogbook.cern.ch/Operations/)
- Has signed up to the [Computing Operations Mattermost channel](https://mattermost.web.cern.ch/lhcb/channels/computing-operations)

More info can be found [here](preparation.md)

## Responsibilities

The DCS is the central point of information, operations and management for LHCb Distributed Computing Operations, in this role they are (in principle) the first contact for everything that concerns LHCb Distributed Computing Operations. The DCS

- Monitors the status of LHCb distributed computing operations including sites, transfers, user problems, etc.
- Manages and possibly solves grid issues where possible, otherwise relays problems to the next line of support
- Provides LHCb specfic information for sites, WLCG Services and to the other members of the general distributed computing teams
- Organises and chairs the LHCb Computing Operations Meetings on Mondays, Wednesdays and Fridays
- Attends the weekly WLCG Operations meetings, gives the LHCb status and reports back any appropriate information to the the Computing
Team.
- If scheduled, attends the monthly WLCG Coordination meeting and again reports back any LHCb-relevant information
- is in charge on updating this guide [in gitlab](https://gitlab.cern.ch/lhcb-dirac/lhcb-dcs-docs)

