# Useful Links

This page provides useful links for the DCS:

- [DIRAC Portal](https://lhcb-portal-dirac.cern.ch/)
- [FTS Monitoring](https://fts3.cern.ch:8449/fts3/ftsmon/#/)
- [LHCb Elog logbook](http://lblogbook.cern.ch/Operations/)
- [LHCb Run Status](https://lbrunstatus.cern.ch/)
- [GGUS](https://ggus.eu/)
- [GOCDB](https://goc.egi.eu/portal/)
- [LHC page 1](http://op-webtools.web.cern.ch/op-webtools/vistar/vistars.php)
- [Mattermost Channel](https://mattermost.web.cern.ch/lhcb/channels/computing-operations)
- [EGI Glossary](https://wiki.egi.eu/wiki/Glossary_V3)
- [LHCb Glossary](https://lhcb.github.io/glossary/)