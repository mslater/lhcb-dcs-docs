# Preparing for a Shift

## Obtaining a CERN Computing Account

This almost goes without saying but the DCS needs to have an active LHCb Linux account at CERN. 
For more info, please see the LHCb VO Manager, Joel Closier.

## Obtaining a Grid Certificate

The DCS must also have a valid Grid Certificate. This does not have to come from the CERN Certification Authority (CA)
but if you don't have a local CA to provide you with one, this would be an easy way to get it. Go to the main
CERN CA page here (<https://ca.cern.ch/ca/>) and select 'New Grid User Certificate'

After you have your certificate, your 'Distinguising Name (DN)' needs to be added to the LHCb Virtual Organisation.
To do this, after you have loaded the certificate in your browser, go to this link <https://lcg-voms2.cern.ch:8443/voms/lhcb>
and request approval. This is a manual process so you may have to wait up to 24 hours.

NOTE: If you're an active member of the LHCb collaboration, you will have already done everything listed above! More detailed information can also be found here: <https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/Certificate>.

## Get Appropriate Roles

Now you have a valid grid certificate registered with the LHCb VO you now need to get authorisation to have elevated privileges
that allow you to see all the information provided by DIRAC. You will need to have the role `lhcb_shifter`
available to you when logging on to the DIRAC portal. To do, please contact the LHCb VO Manager, Joel Closier.

## Subscribe to Mailing Lists

There are several mailling lists, both LHCb and WLCG related that you need to be part of. These can all be joined from the e-groups portal at CERN (<https://e-groups.cern.ch/e-groups/EgroupsSearch.do>)

* ggus-downtimes-lhcb 
* lhcb-distributed-analysis
* lhcb-geoc
* lhcb-grid
* lhcb-grid-alarms
* lhcb-grid-support  [??]
* lhcb-wlcg-coordination 
* VOBox-Admins-LHCb [??]
* Vobox-Lhcb [??]
* VOBox-Responsible-LHCb [??]
* VOBox-VOC-LHCb [??]
* vobox.lhcb [??]
* VOBox-VOC-WLCG [??]
* wlcg-operations
* wlcg-ops-coord [??]
* wlcg-service-coordination [??]

Of these, the most important with respect to the DCS role are the lhcb-geoc and lhcb-grid lists as these are the contact
point for the DCS and the wider group of Grid experts/admins respectively.

## Register in Operations ELog

The Operations ELog can be found here: <https://lblogbook.cern.ch/Operations/>. This is where to record notable events/problems with the
system and sites. Click on 'Login' and then click on 'Register as new user'. 

## Signing up to the Mattermost Channels

The main Mattermost channel for Grid discussions is the Computing Operations in the LHCb Experiment Group. This can be found 
here: <https://mattermost.web.cern.ch/lhcb/channels/computing-operations>. This is usually the first place to report problems and ask
for help/advice.

## Become LHCb Team Member in GGUS

[I don't remember how to do this!]
