# Hosting the LHCb Operations Meetings

The LHCb Computing Operations meetings are held every Monday, Wednesday and Friday at 11:30am CERN time and are setup, chaired
and minuted by the DCS. A remote connection is provided through Zoom. It is used to discuss any matters arising from the previous days, any upcoming interventions, etc. and also get status reports from the Tier 1 sites.

## Setting up the Operations Meeting

The first (and most important!) thing to do for the ops meeting is clone the previous meeting entry in indico. The URL for this can be found in the reminder email that should have been sent to the lhcb-grid email list for the last one or directly from the category in indico (<https://indico.cern.ch/category/4206/>). Do the following:

* Go to the indico page for the previous meeting
* Login if you haven't already
* Click on the 'pencil' icon and select 'Clone Event'
* On the window that opens, select 'Clone Once' and select the appropriate date
* Make sure you select the **material** attached to the agenda is also cloned as this will include some useful links. NOTE: By default this isn't ticked!
* Agree to the cloned meeting
* In the editing page that should have now opened, go to 'Reminders' and the '1 hour before the event' option
* Change the sender field to your email address

If everything has been done correctly you should get the reminder sent from your email and pointing to the correct agenda 1 hour before
the scheduled start (i.e. at 10:30am CERN time)

## Additional Preparations

As well as setting up the meeting agenda page, there are a few other things that should be done. The DCS should go over and gather details of any problems/announcements/etc that are to be discussed. They should also prepare the minutes document based on the template
that can be found here: <https://cernbox.cern.ch/index.php/s/NWQl0Sx0qbfbH0J>

## During the Meeting

At 11:30am (or a few minutes before), do the following:

* Join the Zoom meeting
* If at CERN, phone into the meeting using the conference phone and the details on the agenda page
* Also, if at CERN, plug your laptop into the projector so you can show the agenda, GGUS tickets, plots, etc. as required
* Using Zoom's 'Screen Share' option, share the same view to the remote participants

It can take a few minutes for everyone to join but you should start the meeting by 11:35am. Work through the agenda touching
on each point as required. Minutes of any points raised or general discussions should be taken. They don't have to be exact but
must have the general points so people who can't attend can follow what happened. 

Generally, the meetings will only last 20-30mins (sometimes only 5-10mins!) but if there are big topics being discussed, it can go on for significantly longer. However, it shouldn't last longer than an hour so be aware that you may have to cut off dicsussions if it looks like
you won't get through the agenda in time.

## After the Meeting

As well as any direct actions for the DCS rising from the meeting, you should post a copy of your minutes to the ELog. There is a specific option for 'Operations Minutes'. This will then be sent to the appropriate lists.
