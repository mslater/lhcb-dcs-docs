# Monitoring the System

## Setting up the Dirac Portal for a Shift

There are desktops already setup in the DIRAC portal that have all the appropriate plots for a shift. To access them, do the following:

* With your certificate available in your browser, go to [the DIRAC portal](https://lhcb-portal-dirac.cern.ch/DIRAC/)
* Click on 'Settings' in the menu on the left and make sure that group is set to 'lhcb_shifter'. This allows you to see all job
and pilot information
* Now from the 'Desktop & Applications' menu, select Applications -> Administration -> Public State Manager
* From the desktop that opens, expand 'Desktops' and then 'Shared Desktops'. 
* From here you can load various desktops set up by the team. A good one to start with is 'Shifter Overview' by Federico ('fstagni'). Click on the square on the far right for this desktop.
* Click 'Load' on the window that popped up

You should now have a Shifter Overiew Desktop loaded that contains all the plots you will need during your DCS shift. You can save this
as one of your own desktops as well as make changes as you wish.

## Checks to Make

Now you have setup a DIRAC desktop to monitor the system, you need to regularly check the plots to see if there are any
obvious problems. The main places you will see issues is with failing/aborting pilots and data transfer failures.

### Overview

From this page, you can look at the overall number and state of both production and user jobs as well the pilots. Failed transfers are
also shown. These are general overview plots so are useful to see the overall health of the system.

### Job Execution Rate

These plots break down the running jobs by job type, T1/T2 and state. There should always be running jobs at T1 sites as long as there is
no downtime. There should also generally be both user and MC jobs running as well so if you see any signficant drops that should be reported.

### Job Debugging

These plots show the breakdown of Failed, Completed and Reschsduled jobs sorted according to various factors. A peak of jobs in
Completed status can indicate problems with storage at a particular site. This could also be seen in the reason for Rescheduled jobs,
particularly 'Input Data Resolution', though other issues (e.g. Sandbox problems) can also be seen here.

### WMS Overview

This page gives a breakdown of all jobs according to status and then additionally, by various other metrics. They can be useful to
check for an increases in Waiting jobs (inidcating a problem with the SiteDirector) or a decrease in Running jobs.

### Data Transfer

This page is important to keep checking as it will be the main place you will see significant failures in data management services.
Generally, if there is a problem you will see increases in the 'All Failed Transfers' plots. You can then check the other plots to
see if it's specific to Source, Destination or Activity Type. Note that the Pit Transfers will be blank when we aren't taking data.

### Pilot Jobs Status

Another important page to regularly check, this shows the status of the pilots and most importantly, the number of Aborted and Failed
pilots by site. Any increases for a particular site usually indicates a problem that will often require reporting via GGUS ticket.

### T0/T1 Sites - Running Jobs

This page details the number of running jobs at the Tier 1 sites (and CERN) by type. This can show if there is a low number of jobs
running at a particular Tier 1. This can be just due to fairshre or site downtime/draining but can also indicate a problem with
the Site Director.

### T0/T1 Sites

Similar to the previous page, this shows the jobs at the Tier 1 sites but by status. This can be useful for seeing a peak of Completed
and Waiting jobs.

### HLTFarm Status

When not data taking, the HLT Farm is used for Offline processing and this page is useful to see if there are any particular issues 
with the jobs there. As with the Tier 1 pages, you can check the status for peaks in Completed, Waiting or Failed jobs.

## FTS page

As well as the information from the DIRAC portal pages, it is also worth keeping an eye on the status of the File Transfer System (FTS).
Errors here should show up in the DIRAC Data Transfer plots, but you can get more information about particular failures. The FTS
status page can be found [here](https://fts3.cern.ch:8449/fts3/ftsmon/#/?page=1&dest_se=&vo=lhcb&source_se=&time_window=24).

## Next Lines of Support

If you identified what seems to be a problem, try to gather as much information about it and rule out any simple explanations, e.g.
site in downtime, other service work going on, confined to a particular site, etc. After that, if the problem seems to definitely
be with a particular site, do the following:

- If it is a Tier 1 problem, post a message in the Mattermost channel [here](https://mattermost.web.cern.ch/lhcb/channels/computing-operations) to make everyone aware.
- File a GGUS (<http://ggus.org/>) **team** ticket against them (see below)
- Add an ELog entry [here](http://lblogbook.cern.ch/Operations/) that explains the problem seen and give the GGUS link

If it's a general DIRAC/LHCb problem, then do the following:

- Ask in the Mattermost channel, posting appropriate plots, etc. to explain the problem.
- If noone gets back to you, try emailing the `lhcb-grid` list
- If all else fails, email people directly (e.g. Chris Haen, Vladimir, Federico)

## Submitting a GGUS Ticket

The most common way of informing a site that there is a problem is via a GGUS ticket. To open a ticket against a site or service,
follow these steps:

* go to the [GGUS site](http://ggus.org/) 
* Select 'Submit Ticket'
* Select 'Open TEAM Ticket'
* Fill out the fields. Do try to include as much information as possible.
* Generally, the priority should be 'Very Urgent'

You can now communicate with the site via the ticket and by default, the `lhcb-geoc` mailing list will be copied in so others are aware.
When the problem is solved, the site will post a solution and then you should go to the ticket and click 'Verify Solution' if everything is working again.

On occasions, it may be necessary to submit (or escalate) an ALARM ticket. This should **not** be done lightly as it may involve waking people up or calling them in. That kind of response is very rarely needed and should generally be done after taking advice from other members of the Computing Team.
